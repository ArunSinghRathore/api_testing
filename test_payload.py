import helper


class TestApi:
    name = None
    method = helper.RequestMethod.UNKNOWN
    url = None
    body_payload = {}
    header = {}


apis = []

# Profile Api
profile_api = TestApi()
profile_api.name = "Profile API"
profile_api.url = "profile/pID/"
profile_api.method = helper.RequestMethod.GET
apis.append(profile_api)


# post bookmark api
bookmark_api = TestApi()
bookmark_api.name = "Bookmark Api"
bookmark_api.url = "profile/pID/bookmarked-unit/"
bookmark_api.method = helper.RequestMethod.POST
bookmark_api.body_payload = {
    "unit_id": "uID",
    "is_active": False

}
apis.append(bookmark_api)

# daily help create api

daily_help_api = TestApi()
daily_help_api.name = "DailyHelp Api"
daily_help_api.url = "profile/pID/daily-help-create/"
daily_help_api.method = helper.RequestMethod.POST
daily_help_api.body_payload = {
    "society_id": "sID",
    "units": "1,2",
    "name": "Veer",
    "phone_no": "_rand.10_",
    "aadhaar_no": "_rand.12_",
    "category": 2,
}
# apis.append(daily_help_api)
