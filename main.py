import json
import time
from collections import namedtuple
import requests
import helper
import test_payload

# Declaration

# enable Manual Inputs
enableInputMethod = False

# default user credentials
baseUrl = helper.base_url_is_prod(False)
userPhone = 9818252837
userPassword = 4499
access_token = None
user_guid = None
profiles = []

if enableInputMethod:
    userPhone = input("Enter phone number: ")
    userPassword = input("Enter password: ")

userTokenPayload = {
    "phone": userPhone,
    "password": userPassword
}


# default request headers
def get_default_request_headers():
    return {
        "Authorization": access_token,
        "content-Type": "application/json"
    }


class RequestResponse:
    success = False
    method = helper.RequestMethod.UNKNOWN
    response_type = helper.get_response_type(999).value
    response_time = 0
    url = None
    status_code = 0
    body = None
    message = None


def call_api(method, url, headers=None, payload=None):
    if headers is None:
        headers = {}
    if payload is None:
        payload = {}

    method_call_start_time = time.time()

    if method == helper.RequestMethod.GET:
        return request_response(method, url, requests.get(url, headers=headers), method_call_start_time)

    elif method == helper.RequestMethod.POST:
        return request_response(method, url, requests.post(url, headers=headers, data=payload), method_call_start_time)

    elif method == helper.RequestMethod.PUT:
        return request_response(method, url, requests.put(url, headers=headers, data=payload), method_call_start_time)

    else:
        return request_response(helper.RequestMethod.UNKNOWN, url, result=None,
                                method_call_start_time=method_call_start_time)


def request_response(method, url, result, method_call_start_time):
    print(f"\n\ninitiating url -> {url}")
    response_data = RequestResponse()
    response_time = time.time() - method_call_start_time
    if method != helper.RequestMethod.UNKNOWN:
        success = True if result.status_code in range(200, 204) else False
        response_data.success = success
        response_data.method = method.value
        response_data.response_type = helper.get_response_type(result.status_code).value
        response_data.response_time = response_time
        response_data.url = result.url
        response_data.status_code = result.status_code
        response_data.body = result.text

    else:
        response_data.success = False
        response_data.method = method.value
        response_data.response_type = helper.get_response_type(result.status_code).value
        response_data.response_time = response_time
        response_data.url = result.url
        response_data.status_code = result.status_code
        response_data.body = None
        response_data.message = "Unknown method type.."

    result = response_data
    log(result)
    return result


def log(data):
    print(f"------- Result Log --------")
    print(f"url -> {data.url}")
    print(f"status code -> {data.status_code}")
    print(f"response time -> {data.response_time}")


# getting Access token and userGUID using phone and password
def initiate_api_test():
    global access_token
    global user_guid
    user_token_request = (call_api(helper.RequestMethod.POST, f"{baseUrl}token/", payload=userTokenPayload))
    if user_token_request.success:
        user_token_res = json.loads(user_token_request.body, object_hook=helper.to_object)
        access_token = f"JWT {user_token_res.access}"
        user_guid = user_token_res.data.user_guid
        initiate_staff()
    else:
        print("Please Entre valid Credential")


# fetching Staff Details
def initiate_staff():
    user_staff = call_api(helper.RequestMethod.GET, f"{baseUrl}staff/{user_guid}/",
                          headers=get_default_request_headers())
    if user_staff.success:
        user_staff_res = json.loads(user_staff.body)
        global profiles
        profiles = (user_staff_res["profiles"])

        initiate_testing()

    else:
        print("Error in Loading Staff Api")


# Here collection of TestApi will use to make every single api hit and make log of it.
# with different profile and society using Staff Profiles
#
def initiate_testing():
    for profile in profiles:
        apis = test_payload.apis
        for api in apis:
            url = make_url(f"{baseUrl}{api.url}", profile, True)
            payload = json.dumps(helper.decode_payload_config(api.body_payload, profile))
            call = call_api(api.method, url, headers=get_default_request_headers(), payload=payload)
            if not call.success:
                print(call.body)


# this method is to make or create complete url
# example
# ==================
# http://allcaps.co.in/api/society/sID/ where sID is societyID
# ==================
# hints ----
# sID == societyID
# pID == profileID
# uID == unitID

def make_url(url, profile, with_base_url=False):
    profile_id = profile["id"]
    society_id = profile["society_id"]
    unit_id = profile["unit"]["id"]
    temp_url = url
    temp_url = temp_url.replace("sID", str(society_id))
    temp_url = temp_url.replace("uID", str(unit_id))
    temp_url = temp_url.replace("pID", str(profile_id))
    temp_url = f"{temp_url}" if with_base_url else f"{baseUrl}{temp_url}"
    return temp_url


# this is to start the testing processes
initiate_api_test()
