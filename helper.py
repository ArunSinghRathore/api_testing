import json
from collections import namedtuple
from enum import Enum
from random import randint

# Declaration

society_id_key = "sID"
profile_id_key = "pID"
unit_id_key = "uID"
random_number = "rand_4"


# Methods
def to_object(data): return namedtuple('NestapObject', data.keys())(*data.values())


def base_url_is_prod(is_production=False):
    if is_production:
        return "http://nestap.in/api/"
    else:
        return "http://allcaps.co.in/api/"


def random_with_n_digits(n):
    range_start = 10 ** (n - 1)
    range_end = (10 ** n) - 1
    return randint(range_start, range_end)


def get_response_type(status_code):
    if int(status_code) in range(100, 199):
        return ResponseType.INFORMATIONAL
    elif int(status_code) in range(200, 299):
        return ResponseType.SUCCESSFUL
    elif int(status_code) in range(300, 399):
        return ResponseType.REDIRECTS
    elif int(status_code) in range(400, 499):
        return ResponseType.CLIENT_ERROR
    elif int(status_code) in range(500, 599):
        return ResponseType.SERVER_ERROR
    else:
        return ResponseType.UNKNOWN


# def decode_payload_config(body_payload, profile):


def decode_payload_config(body_payload, profile):
    if isinstance(body_payload, str):
        body_payload = json.loads(body_payload)

    profile_id = profile["id"]
    society_id = profile["society_id"]
    unit_id = profile["unit"]["id"]

    result_temp = {}
    for index in body_payload:
        if isinstance(body_payload[index], dict):
            result_temp[index] = body_payload[index]
            result_temp[index] = decode_payload_config(body_payload[index], profile)
        else:
            temp = str(body_payload[index])
            temp = temp.replace(society_id_key, str(society_id))
            temp = temp.replace(profile_id_key, str(profile_id))
            temp = temp.replace(unit_id_key, str(unit_id))
            temp = decode_random_str(temp)
            result_temp[index] = temp

    return result_temp


def decode_random_str(input_str):
    temp = input_str
    split_rand = (input_str.split("_"))
    for r_word in split_rand:
        try:
            if r_word.find("rand") != -1:
                length = int(r_word.split(".")[1])
                new_length = 1 if (length < 1) else length
                sdf = str(f"_rand.{length}_")
                temp = temp.replace(sdf, str(random_with_n_digits(new_length)))
                break
            else:
                pass
        except:
            pass

    return temp


# Enums
class RequestMethod(Enum):
    GET = "GET"
    POST = "POST"
    PUT = "PUT"
    UNKNOWN = "UNKNOWN"


class ResponseType(Enum):
    INFORMATIONAL = "INFORMATIONAL"
    SUCCESSFUL = "SUCCESSFUL"
    REDIRECTS = "REDIRECTS"
    CLIENT_ERROR = "CLIENT_ERROR"
    SERVER_ERROR = "SERVER_ERROR"
    UNKNOWN = "UNKNOWN"
